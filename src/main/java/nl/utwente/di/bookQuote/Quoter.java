package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;
import static java.util.Map.entry;


public class Quoter {
    private final Map<String, Double> bookPrice =  Map.ofEntries(
            entry("1", 10.0),
            entry("2", 45.0),
            entry("3", 20.0),
            entry("4", 35.0),
            entry("5", 50.0)
    );

    public double getBookPrice(String isbn) {
        return bookPrice.getOrDefault( isbn, 0.0);
    }
}
