package nl.utwente.di;

import nl.utwente.di.bookQuote.Quoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestQuoter {

    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        Assertions.assertEquals(10.0, quoter.getBookPrice("1"), 0.0, "Price of book 1");
        Assertions.assertEquals(45.0, quoter.getBookPrice("2"), 0.0, "Price of book 2");
        Assertions.assertEquals(20.0, quoter.getBookPrice("3"), 0.0, "Price of book 3");
        Assertions.assertEquals(35.0, quoter.getBookPrice("4"), 0.0, "Price of book 4");
        Assertions.assertEquals(50.0, quoter.getBookPrice("5"), 0.0, "Price of book 5");
        Assertions.assertEquals(0.0, quoter.getBookPrice("6"), 0.0, "Price of book 6");

    }
}
